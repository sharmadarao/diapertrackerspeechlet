package diaperTracker;

public class DiaperTrackerTextUtil {
	public static final String DIAPER_TYPE = "diaperType";

	public static final String DIAPER_COUNT = "countNumber";
	
	public static final String FRESH_DIAPER_COUNT = "freshCount";
	
	public static final String TIMELINE = "timeline";
	
}
