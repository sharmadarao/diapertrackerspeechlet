package diaperTracker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.LaunchRequest;
import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SessionEndedRequest;
import com.amazon.speech.speechlet.SessionStartedRequest;
import com.amazon.speech.speechlet.Speechlet;
import com.amazon.speech.speechlet.SpeechletException;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import com.amazon.speech.speechlet.dialog.directives.DelegateDirective;
import com.amazon.speech.speechlet.dialog.directives.DialogIntent;
import com.amazon.speech.speechlet.dialog.directives.DialogSlot;
import com.amazon.speech.speechlet.IntentRequest.DialogState;
import com.amazon.speech.slu.Slot;
import com.amazon.speech.speechlet.Directive;

public class DiaperTrackerSpeechlet implements Speechlet {
	private static final Logger log = LoggerFactory.getLogger(DiaperTrackerSpeechlet.class);

	private AmazonDynamoDBClient amazonDynamoDBClient;

	private SkillContext skillContext;

	private DiaperTrackerManager diaperTrackerManager;

	@Override
	public void onSessionStarted(final SessionStartedRequest request, final Session session) throws SpeechletException {
		log.info("onSessionStarted requestId={}, sessionId={}", request.getRequestId(), session.getSessionId());

		initializeComponents();

		// if user said a one shot command that triggered an intent event,
		// it will start a new session, and then we should avoid speaking too many
		// words.
		skillContext.setNeedsMoreHelp(false);

	}

	@Override
	public SpeechletResponse onLaunch(final LaunchRequest request, final Session session) throws SpeechletException {
		log.info("onLaunch requestId={}, sessionId={}", request.getRequestId(), session.getSessionId());

		skillContext.setNeedsMoreHelp(true);
		return diaperTrackerManager.getLaunchResponse(request);
	}

	@Override
	public void onSessionEnded(final SessionEndedRequest request, final Session session) throws SpeechletException {
		log.info("onSessionEnded requestId={}, sessionId={}", request.getRequestId(), session.getSessionId());
		// any cleanup logic goes here
	}

	@Override
	public SpeechletResponse onIntent(IntentRequest request, Session session) throws SpeechletException {
		log.info("onIntent requestId={}, sessionId={}", request.getRequestId(), session.getSessionId());
		initializeComponents();

		// Get Dialog State
		DialogState dialogueState = request.getDialogState();

		Intent intent = request.getIntent();
		if ("TrackDiaperIntent".equals(intent.getName())) {
			log.info("onIntent intentSlots={}", intent.getSlots());

			// If the IntentRequest dialog state is STARTED
			if (dialogueState.equals(DialogState.STARTED)) {
				Map<String, String> defaults = new HashMap<>();
				defaults.put(DiaperTrackerTextUtil.DIAPER_COUNT, "1");
				return prepareSpeechletResponse(delegateSlotCollection(intent, defaults));
			} else if (dialogueState.equals(DialogState.IN_PROGRESS)) {
				log.debug("onIntent, DIALOG IN_PROGRESS");
				DelegateDirective dd = new DelegateDirective();
				List<Directive> directiveList = new ArrayList<Directive>();
				directiveList.add(dd);
				return prepareSpeechletResponse(directiveList);
			} else {
				log.debug("onIntent, inside else . DialogState:{}", dialogueState);
				return diaperTrackerManager.getAddDiaperResponse(intent, session);
			}

		} else if ("GetStatisticsIntent".equalsIgnoreCase(intent.getName())) {
			if (dialogueState.equals(DialogState.STARTED)) {
				Map<String, String> defaults = new HashMap<>();
				defaults.put(DiaperTrackerTextUtil.TIMELINE, "day");
				return prepareSpeechletResponse(delegateSlotCollection(intent, defaults));
			} else if (dialogueState.equals(DialogState.IN_PROGRESS)) {
				log.debug("onIntent, DIALOG IN_PROGRESS");
				DelegateDirective dd = new DelegateDirective();
				List<Directive> directiveList = new ArrayList<Directive>();
				directiveList.add(dd);
				return prepareSpeechletResponse(directiveList);
			} else {
				log.info("onIntent intentSlots={}", intent.getSlots());
				return diaperTrackerManager.getStatistics(intent, session);
			}

		} else if ("GetLastChangedIntent".equalsIgnoreCase(intent.getName())) {
			if (dialogueState.equals(DialogState.STARTED)) {
				return prepareSpeechletResponse(delegateSlotCollection(intent, new HashMap<String, String>()));
			} else if (dialogueState.equals(DialogState.IN_PROGRESS)) {
				log.debug("onIntent, DIALOG IN_PROGRESS");
				DelegateDirective dd = new DelegateDirective();
				List<Directive> directiveList = new ArrayList<Directive>();
				directiveList.add(dd);
				return prepareSpeechletResponse(directiveList);
			} else {
				log.info("onIntent intentSlots={}", intent.getSlots());
				return diaperTrackerManager.getLastChanged(intent, session);
			}

		} else if ("GetDiaperInventoryIntent".equalsIgnoreCase(intent.getName())) {
			return diaperTrackerManager.getDiaperInventory(session);
		} else if ("SetDiaperInventoryIntent".equalsIgnoreCase(intent.getName())) {

			return diaperTrackerManager.setDiaperInventory(intent, session);
		}

		else if ("AMAZON.HelpIntent".equals(intent.getName())) {
			return diaperTrackerManager.getHelpIntentResponse(intent, skillContext);
		} else {
			throw new IllegalArgumentException("Unrecognized intent: " + intent.getName());
		}
	}

	private static List<Directive> delegateSlotCollection(Intent intent, Map<String, String> defaults) {
		log.debug("Building DialogIntent");
		DialogIntent dialogIntent = new DialogIntent();
		dialogIntent.setName(intent.getName());

		Map<String, DialogSlot> dialogSlots = new HashMap<String, DialogSlot>();
		Iterator iter = intent.getSlots().entrySet().iterator();

		while (iter.hasNext()) {
			Map.Entry pair = (Map.Entry) iter.next();
			DialogSlot dialogSlot = new DialogSlot();
			log.info("PAIR: {}, {}", pair.getKey(), pair.getValue());
			Slot slot = (Slot) pair.getValue();
			dialogSlot.setName(slot.getName());
			log.info("SLOT : {}, {}", slot.getName(), slot.getValue());

			if (defaults.containsKey(slot.getName()) && slot.getValue() == null) {
				dialogSlot.setValue(defaults.get(slot.getName()));
				log.info("onIntent: setting count to:{}", dialogSlot.getValue());
			}
			if (slot.getValue() != null) {
				dialogSlot.setValue(slot.getValue());
				log.info("onIntent: retaining value for: {}, {}", slot.getName(), slot.getValue());
			}
			dialogSlots.put((String) pair.getKey(), dialogSlot);

			log.debug("DialogSlot New" + dialogSlot.getName() + " with value " + dialogSlot.getValue() + " added.");

		}
		dialogIntent.setSlots(dialogSlots);

		DelegateDirective dd = new DelegateDirective();
		dd.setUpdatedIntent(dialogIntent);
		List<Directive> directiveList = new ArrayList<Directive>();
		directiveList.add(dd);
		return directiveList;

	}

	private SpeechletResponse prepareSpeechletResponse(List<Directive> directiveList) {
		SpeechletResponse speechletResp = new SpeechletResponse();
		speechletResp.setDirectives(directiveList);
		speechletResp.setNullableShouldEndSession(false);
		return speechletResp;
	}

	/**
	 * Initializes the instance components if needed.
	 */
	private void initializeComponents() {
		if (amazonDynamoDBClient == null) {
			amazonDynamoDBClient = new AmazonDynamoDBClient();
			diaperTrackerManager = new DiaperTrackerManager(amazonDynamoDBClient);
			skillContext = new SkillContext();
		}
	}

}
