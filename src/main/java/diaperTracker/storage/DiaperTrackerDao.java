package diaperTracker.storage;

import com.amazon.speech.speechlet.Session;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Contains the methods to interact with the persistence layer for DiaperTracker
 * in DynamoDB.
 */
public class DiaperTrackerDao {
    private final DiaperTrackerDynamoDbClient dynamoDbClient;
    private final String tableName = "DiaperTrackerUsageData";
    private final String inventoryTableName = "DiaperInventory";
    private static final Logger log = LoggerFactory.getLogger(DiaperTrackerDao.class);

    public DiaperTrackerDao(DiaperTrackerDynamoDbClient dynamoDbClient) {
        this.dynamoDbClient = dynamoDbClient;
    }

    public List<DiaperUsageData> getDiaperUsageDataBetweenDates(String fromDate, String toDate, Session session) {
        QuerySpec qspec = new QuerySpec()
                .withKeyConditionExpression("customerId = :customerId and changeDate between :fromDate and :toDate")
                .withValueMap(new ValueMap()
                        .withString(":fromDate", fromDate)
                        .withString(":toDate", toDate)
                        .withString(":customerId", session.getUser().getUserId()))
                .withProjectionExpression("dusage");

        ItemCollection<QueryOutcome> items = dynamoDbClient.queryTable(qspec, tableName);

        Iterator<Item> iterator = items.iterator();
        List<DiaperUsageData> usageStats = new LinkedList<DiaperUsageData>();
        while (iterator.hasNext()) {
            Item item = iterator.next();
            if (item == null) {
                return null;
            }

            String j = item.toJSON();
            if (j == null || j.isEmpty()) {
                return null;
            }

            JsonObject jo = (JsonObject) new JsonParser().parse(j);
            JsonObject djo = (JsonObject) jo.get("dusage");
            DiaperUsageData di = new Gson().fromJson(djo, DiaperUsageData.class);
            usageStats.add(di);
        }
        return usageStats;

    }

    public void saveDiaperUsageData(DiaperUsageData diaperUsage, Session session) {
        String changeDate = LocalDate.now().toString();

        Item putItemSpec = new Item().withPrimaryKey("customerId", session.getUser().getUserId(), "changeDate", changeDate).withJSON("dusage",
                new Gson().toJson(diaperUsage));

        dynamoDbClient.saveItem(putItemSpec, tableName);
    }

    public void setInventory(Integer count, Session session) {
        Item putItemSpec = new Item().withPrimaryKey("customerId", session.getUser().getUserId()).withNumber("fcount", count);
        dynamoDbClient.saveItem(putItemSpec, inventoryTableName);
    }

    public int getInventory(Session session) {
        GetItemSpec getItemSpec = new GetItemSpec()
                .withPrimaryKey("customerId", session.getUser().getUserId()).withProjectionExpression("fcount");
        Item item = dynamoDbClient.loadItem(getItemSpec, inventoryTableName);
        if (item == null) {
            return 0;
        }
        log.info("fresh count: {}", item.getBigInteger("fcount"));
        return item.getBigInteger("fcount").intValue();
    }
}
