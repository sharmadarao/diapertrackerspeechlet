package diaperTracker.storage;

public class DiaperType {
	private int count;
	private String lastChanged;

	public static DiaperType newInstance() {
		DiaperType newInstance = new DiaperType();
		newInstance.setCount(0);
		newInstance.setLastChanged(null);
		return newInstance;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getLastChanged() {
		return lastChanged;
	}

	public void setLastChanged(String lastChanged) {
		this.lastChanged = lastChanged;
	}
	
	public String toString() {
		return "count:" + count + " lastchanged:" + lastChanged;
	}

}