package diaperTracker.storage;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMarshaller;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMarshalling;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.JsonMarshaller;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@DynamoDBTable(tableName = "DiaperTrackerData")
public class DiaperTrackerUsageItem {
	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private String changeDate;
	private DiaperUsageData diaperUsageData;

	@DynamoDBHashKey(attributeName = "changeDate")
	public String getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(String changeDate) {
		this.changeDate = changeDate;
	}

	@DynamoDBAttribute(attributeName = "usage")
	@DynamoDBMarshalling(marshallerClass = DiaperTrackerDataMarshaller.class)
	public DiaperUsageData getDiaperUsageData() {
		return diaperUsageData;
	}

	public void setDiaperUsageData(DiaperUsageData diaperUsageData) {
		this.diaperUsageData = diaperUsageData;
	}

	public static ObjectMapper getObjectMapper() {
		return OBJECT_MAPPER;
	}

	/**
	 * A {@link DynamoDBMarshaller} that provides marshalling and unmarshalling
	 * logic for {@link ScoreKeeperGameData} values so that they can be persisted in
	 * the database as String.
	 */
	public static class DiaperTrackerDataMarshaller implements DynamoDBMarshaller<DiaperUsageData> {

		@Override
		public String marshall(DiaperUsageData diaperData) {
			try {
				return OBJECT_MAPPER.writeValueAsString(diaperData);
			} catch (JsonProcessingException e) {
				throw new IllegalStateException("Unable to marshall diaper data", e);
			}
		}

		@Override
		public DiaperUsageData unmarshall(Class<DiaperUsageData> clazz, String value) {
			try {
				return OBJECT_MAPPER.readValue(value, new TypeReference<DiaperUsageData>() {
				});
			} catch (Exception e) {
				throw new IllegalStateException("Unable to unmarshall diaper data value", e);
			}
		}
	}
}
