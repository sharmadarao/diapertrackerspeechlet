package diaperTracker.storage;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;

public class DiaperTrackerDynamoDbClient {
    private final AmazonDynamoDBClient dynamoDBClient;
    private final DynamoDB dynamoDb;

    public DiaperTrackerDynamoDbClient(final AmazonDynamoDBClient dynamoDBClient) {
        this.dynamoDBClient = dynamoDBClient;
        this.dynamoDb = new DynamoDB(dynamoDBClient);
    }

    /**
     * Loads an item from DynamoDB by primary Hash Key. Callers of this method
     * should pass in an object which represents an item in the DynamoDB table item
     * with the primary key populated.
     *
     * @param tableItem
     * @return
     */
    public Item loadItem(GetItemSpec getItemSpec, String tableName) {
        Table table = dynamoDb.getTable(tableName);
        return table.getItem(getItemSpec);

    }

    public ItemCollection<QueryOutcome> queryTable(QuerySpec queryItemSpec, String tableName) {
        Table table = dynamoDb.getTable(tableName);
        return table.query(queryItemSpec);
    }

    /**
     * Stores an item to DynamoDB.
     *
     * @param tableItem
     */
    public void saveItem(Item item, String tableName) {
        Table table = dynamoDb.getTable(tableName);
        table.putItem(item);
    }

    /**
     * Creates a {@link DynamoDBMapper} using the default configurations.
     *
     * @return
     */
    private DynamoDBMapper createDynamoDBMapper() {
        return new DynamoDBMapper(dynamoDBClient);
    }

}
