package diaperTracker.storage;

public class DiaperUsageData {
	private DiaperType wet;
	private DiaperType dirty;
	private DiaperType both;

	public DiaperUsageData() {
		// public no-arg constructor required for DynamoDBMapper marshalling
	}

	public static DiaperUsageData newInstance() {
		DiaperUsageData newInstance = new DiaperUsageData();
		newInstance.setWet(DiaperType.newInstance());
		newInstance.setDirty(DiaperType.newInstance());
		newInstance.setBoth(DiaperType.newInstance());
		return newInstance;
	}

	public DiaperType getWet() {
		return wet;
	}

	public void setWet(DiaperType wet) {
		this.wet = wet;
	}

	public DiaperType getDirty() {
		return dirty;
	}

	public void setDirty(DiaperType dirty) {
		this.dirty = dirty;
	}

	public DiaperType getBoth() {
		return both;
	}

	public void setBoth(DiaperType both) {
		this.both = both;
	}
	
	public String toString() {
		return "WET:" + wet + "|" + "DIRTY" + dirty +  "|" +  "BOTH" + both ;
	}
}
