package diaperTracker;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.LaunchRequest;
import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.Reprompt;
import com.amazon.speech.ui.SimpleCard;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;

import diaperTracker.storage.DiaperTrackerDao;
import diaperTracker.storage.DiaperTrackerDynamoDbClient;
import diaperTracker.storage.DiaperType;
import diaperTracker.storage.DiaperUsageData;

public class DiaperTrackerManager {

	private static final Logger log = LoggerFactory.getLogger(DiaperTrackerManager.class);

	private final DiaperTrackerDao diaperTrackerDao;

	public DiaperTrackerManager(final AmazonDynamoDBClient amazonDynamoDbClient) {
		DiaperTrackerDynamoDbClient dynamoDbClient = new DiaperTrackerDynamoDbClient(amazonDynamoDbClient);
		diaperTrackerDao = new DiaperTrackerDao(dynamoDbClient);
	}

	public SpeechletResponse getLaunchResponse(LaunchRequest request) {
		String speechText = "what do you want to do today?";
		String repromptText = "You can add a diaper, get usage stats or check your inventory";
		return getAskSpeechletResponse(speechText, repromptText);
	}

	public SpeechletResponse getAddDiaperResponse(Intent intent, Session session) {
		String diaperType = intent.getSlot(DiaperTrackerTextUtil.DIAPER_TYPE).getValue();
		int count = Integer.parseInt(intent.getSlot(DiaperTrackerTextUtil.DIAPER_COUNT).getValue());
		String dateNow = LocalDate.now().toString();
		List<DiaperUsageData> usageList = diaperTrackerDao.getDiaperUsageDataBetweenDates(dateNow, dateNow, session);

		DiaperUsageData usage;
		if (usageList.isEmpty()) {
			usage = DiaperUsageData.newInstance();
		} else {
			usage = usageList.get(0);
		}
		log.info("getAddDiaperResponse:currentUsage={}", usage.toString());

		if ("WET".equalsIgnoreCase(diaperType)) {
			DiaperType currentStatus = usage.getWet();
			currentStatus = updateUsageStatus(currentStatus, count);
			usage.setWet(currentStatus);
		} else if ("DIRTY".equalsIgnoreCase(diaperType)) {
			DiaperType currentStatus = usage.getDirty();
			currentStatus = updateUsageStatus(currentStatus, count);
			usage.setDirty(currentStatus);
		} else if ("BOTH".equalsIgnoreCase(diaperType)) {
			DiaperType currentStatus = usage.getBoth();
			currentStatus = updateUsageStatus(currentStatus, count);
			usage.setBoth(currentStatus);
		} else {
			String repromptText = "I do not understand that diaper type. Diaper types I know are wet, dirty or both";
			return getTellSpeechletResponse(repromptText);
		}

		diaperTrackerDao.saveDiaperUsageData(usage, session);
		diaperTrackerDao.setInventory(diaperTrackerDao.getInventory(session) - count, session);
		String speechText = "Added " + count + " " + diaperType + " " + "diaper";
		return getTellSpeechletResponse(speechText);
	}

	public SpeechletResponse getStatistics(Intent intent, Session session) {
		String diaperType = intent.getSlot(DiaperTrackerTextUtil.DIAPER_TYPE).getValue();
		String timeLine = intent.getSlot(DiaperTrackerTextUtil.TIMELINE).getValue();
		if (diaperType == null) {
			diaperType = "";
		}
		String nowDate = LocalDate.now().toString();
		String toDate = null;
		if (timeLine.equals("month")) {
			toDate = calculateDateRange(30L);
		} else if (timeLine.equals("week")) {
			toDate = calculateDateRange(7L);
		} else {
			toDate = LocalDate.now().toString();
		}
		List<DiaperUsageData> usage = diaperTrackerDao.getDiaperUsageDataBetweenDates(toDate, nowDate, session);
		log.info("getStatistics: type to diaper={}", diaperType);

		int count = 0;
		if (usage == null || usage.isEmpty()) {
			String speechText = "Your baby has zero " + diaperType + "diapers this " + timeLine;
			return getTellSpeechletResponse(speechText);
		}

		if ("WET".equalsIgnoreCase(diaperType)) {
			for (DiaperUsageData u : usage) {
				count += u.getWet().getCount();
			}
		} else if ("DIRTY".equalsIgnoreCase(diaperType)) {
			for (DiaperUsageData u : usage) {
				count += u.getDirty().getCount();
			}
		} else if ("BOTH".equalsIgnoreCase(diaperType)) {
			for (DiaperUsageData u : usage) {
				count += u.getBoth().getCount();
			}
		} else {
			for (DiaperUsageData u : usage) {
				count += u.getWet().getCount() + u.getDirty().getCount() + u.getBoth().getCount();
			}

		}
		String responseText = "Your baby has " + count + " " + diaperType + "diapers this " + timeLine;
		return getTellSpeechletResponse(responseText);
	}

	public SpeechletResponse getLastChanged(Intent intent, Session session) {
		String diaperType = intent.getSlot(DiaperTrackerTextUtil.DIAPER_TYPE).getValue();
		String dateNow = LocalDate.now().toString();
		List<DiaperUsageData> usageList = diaperTrackerDao.getDiaperUsageDataBetweenDates(dateNow, dateNow, session);

		log.info("getLastChanged: type to diaper={}", diaperType);

		if (usageList.isEmpty()) {
			String speechText = "You have not changed any " + diaperType + "diapers today";
			return getTellSpeechletResponse(speechText);
		}

		DiaperUsageData usage = usageList.get(0);
		String lastChanged = null;
		if ("WET".equalsIgnoreCase(diaperType)) {
			lastChanged = usage.getWet().getLastChanged();
		} else if ("DIRTY".equalsIgnoreCase(diaperType)) {
			lastChanged = usage.getDirty().getLastChanged();
		} else if ("BOTH".equalsIgnoreCase(diaperType)) {
			lastChanged = usage.getBoth().getLastChanged();
		} else {
			String repromptText = "I do not understand that diaper type. Diaper types I know are wet, dirty or both";
			return getTellSpeechletResponse(repromptText);
		}
		if (lastChanged == null) {
			String speechText = "You have not changed any " + diaperType + "diapers today";
			return getTellSpeechletResponse(speechText);
		}
		DateFormat sdf = new SimpleDateFormat("hh:mm a");
		Date date = null;
		try {
			date = sdf.parse(lastChanged);
		} catch (ParseException e) {
			String responseText = "oops there was a problem";
			return getTellSpeechletResponse(responseText);
		}
		String responseText = "The last " + diaperType + " diaper you changed was at " + sdf.format(date);
		return getTellSpeechletResponse(responseText);
	}

	public SpeechletResponse getDiaperInventory(Session session) {
		int count = diaperTrackerDao.getInventory(session);
		String responseText = "There are " + count + " fresh diapers";
		return getTellSpeechletResponse(responseText);
	}

	public SpeechletResponse setDiaperInventory(Intent intent, Session session) {
		int count = Integer.parseInt(intent.getSlot(DiaperTrackerTextUtil.FRESH_DIAPER_COUNT).getValue());
		diaperTrackerDao.setInventory(count, session);
		String responseText = "added " + count + " fresh diapers to inventory";
		return getTellSpeechletResponse(responseText);
	}

	private String calculateDateRange(Long numberOfDays) {
		long daysAgoInMillis = (new Date()).getTime() - (numberOfDays * 24L * 60L * 60L * 1000L);
		Date daysAgo = new Date();
		daysAgo.setTime(daysAgoInMillis);
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

		return dateFormatter.format(daysAgo);
	}

	/**
	 * Creates and returns response for the help intent.
	 *
	 * @param intent
	 *            {@link Intent} for this request
	 * 
	 * @param skillContext
	 *            {@link SkillContext} for this request
	 * @return response for the help intent
	 */
	public SpeechletResponse getHelpIntentResponse(Intent intent, SkillContext skillContext) {
		return skillContext.needsMoreHelp()
				? getAskSpeechletResponse("you can add diaper, ask for stats" + " So, how can I help?",
						"you can say add a wet, dirty or both " + "What would you like?")
				: getTellSpeechletResponse("you can add diaper, ask for stats" + " So, how can I help?");
	}

	/**
	 * Returns an ask Speechlet response for a speech and reprompt text.
	 *
	 * @param speechText
	 *            Text for speech output
	 * @param repromptText
	 *            Text for reprompt output
	 * @return ask Speechlet response for a speech and reprompt text
	 */
	private SpeechletResponse getAskSpeechletResponse(String speechText, String repromptText) {
		// Create the Simple card content.
		SimpleCard card = new SimpleCard();
		card.setTitle("Diaper Tracking");
		card.setContent(speechText);

		// Create the plain text output.
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(speechText);

		// Create reprompt
		PlainTextOutputSpeech repromptSpeech = new PlainTextOutputSpeech();
		repromptSpeech.setText(repromptText);
		Reprompt reprompt = new Reprompt();
		reprompt.setOutputSpeech(repromptSpeech);

		return SpeechletResponse.newAskResponse(speech, reprompt, card);
	}

	/**
	 * Returns a tell Speechlet response for a speech and reprompt text.
	 *
	 * @param speechText
	 *            Text for speech output
	 * @return a tell Speechlet response for a speech and reprompt text
	 */
	private SpeechletResponse getTellSpeechletResponse(String speechText) {
		// Create the Simple card content.
		SimpleCard card = new SimpleCard();
		card.setTitle("Diaper Tracking");
		card.setContent(speechText);

		// Create the plain text output.
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(speechText);

		return SpeechletResponse.newTellResponse(speech, card);
	}

	private DiaperType updateUsageStatus(DiaperType currentStatus, int count) {
		currentStatus.setCount(currentStatus.getCount() + count);

		String time = LocalTime.now().format(DateTimeFormatter.ofPattern("hh:mm a"));
		currentStatus.setLastChanged(time);
		return currentStatus;
	}

}
